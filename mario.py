from cs50 import get_int

while True:
    height = get_int("Height: ")
    if 0 <= height <= 23:
        break

for i in range(height):
    print((height - i - 1) * " ", end="")
    print((i + 2) * "#")
